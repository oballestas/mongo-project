"""
Mongo DB - CAR LSV
By: Óscar Ballestas Ortega
"""

from bson.objectid import ObjectId  # noqa
from car.databases.client import MongoLsv
mongo_lsv = MongoLsv()


# Create Records

""" print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_test",
        collection="users",
        record={"name": "Óscar", "last_name": "Ballestas"},
    )
)  """


# Update Records

""" print(
    mongo_lsv.update_record_in_collection(
        db_name="car_test",
        collection="users",
        record_query={"_id": ObjectId("62abd5e19c194650ce70cf22")},
        record_new_value={"last_name": "Ballestas Ortega"},
    )
) """


# Delete Records

""" print(
    mongo_lsv.delete_record_in_collection(
        db_name="car_test",
        collection="users",
        record_id="62abd93ca2897f347df4290c",
    )
) """


print("-------------------------")
print("Databases:")
print(mongo_lsv.list_dbs())
print("-------------------------")
print("Collections:")
print(mongo_lsv.list_collections(db_name="car_test"))
print("-------------------------")
print("Records:")
print(
    mongo_lsv.get_records_from_collection(
        db_name="car_test", collection="users"
    )
)