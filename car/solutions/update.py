from bson.objectid import ObjectId
import sys
sys.path.append("..")
from databases.client import MongoLsv

mongo_lsv = MongoLsv()


"""
    Use this code to update records in the programming_language and programmer collections
    of the car_lsv database.
"""

print(
    mongo_lsv.update_record_in_collection(
        db_name="car_lsv",
        collection="programming_language",
        record_query={"_id": ObjectId("62ae04435491a9df8fe9090f")},
        record_new_value={"name": "php"},
    )
) 

print(
    mongo_lsv.update_record_in_collection(
        db_name="car_lsv",
        collection="programming_language",
        record_query={"_id": ObjectId("62ae04435491a9df8fe9090e")},
        record_new_value={"name": "c++"},
    )
) 


print(
    mongo_lsv.update_record_in_collection(
        db_name="car_lsv",
        collection="programmer",
        record_query={"_id": ObjectId("62ae1127972f79b190b205c1")},
        record_new_value={"last_name": "Ballestas Ortega"},
    )
) 

print(
    mongo_lsv.update_record_in_collection(
        db_name="car_lsv",
        collection="programmer",
        record_query={"_id": ObjectId("62ae1127972f79b190b205c2")},
        record_new_value={"programming_language":ObjectId("62ae04435491a9df8fe9090d")},
    )
) 


# Getting the dbs,collections and records

print("-------------------------")
print("Databases:")
print(mongo_lsv.list_dbs())
print("-------------------------")
print("Collections:")
print(mongo_lsv.list_collections(db_name="car_lsv"))
print("-------------------------")
print("Records of programming_language:")
print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="programming_language"
    )
)
print("-------------------------")
print("Records of programmer:")
print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="programmer"
    )
)