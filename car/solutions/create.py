from bson.objectid import ObjectId
import sys
sys.path.append("..")
from databases.client import MongoLsv
mongo_lsv = MongoLsv()

"""
    Use this code to insert records in the programming_language and programmer collections
    of the car_lsv database.
"""


# Creating programming languages records

""" print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="programming_language",
        record={"name": "Python"},
    )
)  

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="programming_language",
        record={"name": "JavaScript"},
    )
)  

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="programming_language",
        record={"name": "C++"},
    )
)  

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="programming_language",
        record={"name": "PHP"},
    )
) 

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="programming_language",
        record={"name": "C#"},
    )
)   """

# Creating programmers records

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="programmer",
        record={"name": "Óscar","last_name":"Ballestas",
                "programming_language":[ObjectId("62ae04435491a9df8fe9090c"),
                                        ObjectId("62ae04435491a9df8fe9090d")],
                }
    )
)  
print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="programmer",
        record={"name": "Erick","last_name":"Contreras",
                "programming_language":[ObjectId("62ae04435491a9df8fe9090c"),
                                        ObjectId("62ae04435491a9df8fe9090f")]},
    )
)  

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="programmer",
        record={"name": "Angélica","last_name":"Morales",
                "programming_language":[ObjectId("62ae04435491a9df8fe9090f"),
                                        ObjectId("62ae04435491a9df8fe90910")]},
    )
)  

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="programmer",
        record={"name": "Luis","last_name":"Payares",
                "programming_language":[ObjectId("62ae04435491a9df8fe9090c"),
                                        ObjectId("62ae04435491a9df8fe90910")]},
    )
)  

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="programmer",
        record={"name": "Juan","last_name":"Sierra",
                "programming_language":[ObjectId("62ae04435491a9df8fe9090d"),
                                        ObjectId("62ae04435491a9df8fe9090c")]},
    )
)   


# Getting the dbs,collections and records

print("-------------------------")
print("Databases:")
print(mongo_lsv.list_dbs())
print("-------------------------")
print("Collections:")
print(mongo_lsv.list_collections(db_name="car_lsv"))
print("-------------------------")
print("Records of programming_language:")
print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="programming_language"
    )
)
print("-------------------------")
print("Records of programmer:")
print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="programmer"
    )
)