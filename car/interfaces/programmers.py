from dataclasses import asdict, dataclass

@dataclass
class ProgrammingLanguage:
    uuid: str
    name: str

    programming_language_record = {"name":"Python"}

    def to_dict(self) -> dict:
        return asdict(self)

@dataclass
class Programmer:
    uuid: str
    name: str
    last_name: str
    programming_language_id: str

    programmer_record = {"name":"Óscar","last_name":"Ballestas","programming_language_id":"mongo_id"}

    def to_dict(self) -> dict:
        return asdict(self)