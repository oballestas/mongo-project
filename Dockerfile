FROM mongo:latest

RUN mkdir /backend /var/secrets
WORKDIR backend

COPY . .

# install Python 3
RUN apt-get update && apt-get install -y python3 python3-pip
RUN apt-get -y install python3.7
RUN pip install -r requirements.txt

EXPOSE 27017